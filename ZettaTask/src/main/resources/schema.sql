create table if not exists conversions
(
    id                int auto_increment
    primary key,
    source_currencies varchar(3) not null,
    target_currencies varchar(3) not null,
    exchange_rates     double     not null,
    source_amounts     double     not null,
    target_amounts     double     not null,
    dates        date   not null,
    time_stamps time not null
    );

