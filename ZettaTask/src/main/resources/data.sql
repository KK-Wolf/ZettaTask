INSERT INTO conversions (id, source_currencies, target_currencies, exchange_rates, source_amounts, target_amounts, dates, time_stamps)
VALUES
    (1, 'USD', 'EUR', 0.85, 100.00, 85.00, '2024-03-23', '12:00:00'),
    (2, 'EUR', 'GBP', 0.88, 200.00, 176.00, '2024-03-23', '12:15:00'),
    (3, 'GBP', 'JPY', 150.45, 300.00, 45135.00, '2024-03-23', '12:30:00'),
    (4, 'JPY', 'AUD', 0.0072, 400.00, 2.88, '2024-03-23', '12:45:00'),
    (5, 'AUD', 'CAD', 0.94, 500.00, 470.00, '2024-03-23', '13:17:00'),
    (6, 'CAD', 'CHF', 0.78, 600.00, 468.00, '2024-03-23', '14:11:07'),
    (7, 'CHF', 'INR', 73.82, 700.00, 51774.00, '2024-03-23', '10:10:10'),
    (8, 'INR', 'CNY', 0.083, 800.00, 66.40, '2024-03-23', '18:05:08'),
    (9, 'CNY', 'BRL', 0.18, 900.00, 162.00, '2024-03-23', '12:20:47'),
    (10, 'BRL', 'USD', 0.20, 1000.00, 200.00, '2024-03-23' , '19:37:14');
