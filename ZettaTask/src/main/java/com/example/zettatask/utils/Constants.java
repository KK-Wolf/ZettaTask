package com.example.zettatask.utils;

public final class Constants {
    private Constants(){}

    public static final String NO_TRANSACTIONS_FOR_THE_PROVIDED_PARAMETERS = "No transactions for the provided parameters";
    public static final String CONVERSION_WITH_ID = "Conversion with ID ";
    public static final String NOT_FOUND = " not found";
    public static final String ONE_OF_PARAMETERS_TRANSACTION_ID_AND_TRANSACTION_DATE_MUST_BE_PROVIDED = "At least one of parameters transactionId and transactionDate must be provided.";
    public static final String INPUT_FORMAT_IS_INVALID = "Input format is invalid";
    public static final String RESULT = "result";
    public static final String SUCCESS = "success";
    public static final String FAILED_TO_FETCH_EXCHANGE_RATE_RESULT = "Failed to fetch exchange rate. Result: ";
    public static final String CONVERSION_RATE = "conversion_rate";
    public static final String EXTERNAL_API_URL = "https://v6.exchangerate-api.com/v6/560802502e0ef47f7a7008e4/pair/";
    public static final String S_WITH_S_S_NOT_FOUND = "%s with %s %s not found";
    public static final String CURRENCY_PAIR_NOT_FOUND = "Currency pair for %s and %s not found";

}
