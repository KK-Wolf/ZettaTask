package com.example.zettatask.utils.exceptions;

import static com.example.zettatask.utils.Constants.*;

public class UnsupportedCurrencyPairException extends RuntimeException {

    public UnsupportedCurrencyPairException(String firstCurrency, String secondCurrency) {
        super(String.format(CURRENCY_PAIR_NOT_FOUND, firstCurrency, secondCurrency));
    }
    public UnsupportedCurrencyPairException(String message){
        super(message);
    }
}
