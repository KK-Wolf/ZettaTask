package com.example.zettatask.utils.exceptions;

import static com.example.zettatask.utils.Constants.*;

public class EntityNotFoundException extends RuntimeException {


    public EntityNotFoundException() {

    }

    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(String type, String name) {
        this(type, "name", name);
    }

    public EntityNotFoundException(String type, int id) {
        this(type, "id", String.valueOf(id));
    }

    public EntityNotFoundException(String type, String attribute, String id) {
        super(String.format(S_WITH_S_S_NOT_FOUND, type, attribute, id));
    }
}

