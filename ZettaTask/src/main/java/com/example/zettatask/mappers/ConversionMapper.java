package com.example.zettatask.mappers;

import com.example.zettatask.models.Conversion;
import com.example.zettatask.models.dto.PageDtoOut;
import com.example.zettatask.models.dto.SingleConversionDto;
import com.example.zettatask.models.dto.SingleConversionDtoOut;
import com.example.zettatask.services.ConversionService;
import com.example.zettatask.utils.exceptions.UnsupportedCurrencyPairException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

import static com.example.zettatask.utils.Constants.*;

@Component
public class ConversionMapper {

    private final ConversionService conversionService;

    @Autowired
    public ConversionMapper(ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    public Conversion fromDto(SingleConversionDto singleConversionDto) {
        try {
            Conversion conversion = new Conversion();
            conversion.setSourceCurrency(singleConversionDto.getSourceCurrency());
            conversion.setTargetCurrency(singleConversionDto.getTargetCurrency());
            conversion.setSourceAmount(singleConversionDto.getSourceAmount());
            conversion.setTransactionDate(LocalDateTime.now().toLocalDate());
            conversion.setTimeStamp(LocalDateTime.now().toLocalTime());
            double currentRate = conversionService.getCurrentExchangeRateBetween(singleConversionDto.getSourceCurrency(),
                    singleConversionDto.getTargetCurrency());
            conversion.setExchangeRate(currentRate);
            conversion.setTargetAmount(currentRate * singleConversionDto.getSourceAmount());

            return conversion;
        } catch (HttpMessageNotReadableException e) {
            throw new UnsupportedCurrencyPairException(INPUT_FORMAT_IS_INVALID);
        }

    }

    public SingleConversionDtoOut toDto(Conversion conversion) {
        SingleConversionDtoOut result = new SingleConversionDtoOut();
        result.setTransactionId(conversion.getId());
        result.setTargetCurrency(conversion.getTargetCurrency());
        result.setTargetAmount(conversion.getTargetAmount());

        return result;
    }

    public PageDtoOut fromPages(Page<Conversion> pages) {
        return new PageDtoOut(pages.getContent(), pages.getNumber(), pages.getSize(), pages.getNumberOfElements(),
                pages.getTotalElements(), pages.getTotalPages());
    }

}
