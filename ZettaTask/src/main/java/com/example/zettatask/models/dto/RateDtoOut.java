package com.example.zettatask.models.dto;

public class RateDtoOut {
    private double currentRate;

    public RateDtoOut() {
    }

    public double getCurrentRate() {
        return currentRate;
    }

    public void setCurrentRate(double currentRate) {
        this.currentRate = currentRate;
    }
}
