package com.example.zettatask.models.dto;

import com.example.zettatask.models.Conversion;

import java.util.List;

public class PageDtoOut {
    private List<Conversion> content;
    private int pageNumber;
    private int pageSize;
    private int elementsOnPage;
    private long totalElements;
    private int totalPages;

    public PageDtoOut() {
    }

    public PageDtoOut(List<Conversion> content, int pageNumber, int pageSize, int elementsOnPage, long totalElements, int totalPages) {
        this.content = content;
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.elementsOnPage = elementsOnPage;
        this.totalElements = totalElements;
        this.totalPages = totalPages;
    }

    public List<Conversion> getContent() {
        return content;
    }

    public void setContent(List<Conversion> content) {
        this.content = content;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getElementsOnPage() {
        return elementsOnPage;
    }

    public void setElementsOnPage(int elementsOnPage) {
        this.elementsOnPage = elementsOnPage;
    }
}
