package com.example.zettatask.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.Positive;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

@Entity
@Table(name = "conversions")
public class Conversion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "source_currencies")
    @Length(min = 3, max = 3)
    private String sourceCurrency;
    @Column(name = "target_currencies")
    @Length(min = 3, max = 3)
    private String targetCurrency;
    @Column(name = "exchange_rates")
    @Positive(message = "Exchange rate should be positive")
    private double exchangeRate;
    @Column(name = "source_amounts")
    @Positive(message = "Source amount should be positive")
    private double sourceAmount;
    @Column(name = "target_amounts")
    @Positive(message = "Target amount should be positive")
    private double targetAmount;
    @Column(name = "dates")
    private LocalDate transactionDate;
    @Column(name = "time_stamps")
    private LocalTime timeStamp;

    public Conversion() {
    }

    public Conversion(int id, String sourceCurrency, String targetCurrency, double exchangeRate,
                      double sourceAmount, LocalDate transactionDate, LocalTime timeStamp) {
        this.id = id;
        this.sourceCurrency = sourceCurrency;
        this.targetCurrency = targetCurrency;
        this.exchangeRate = exchangeRate;
        this.sourceAmount = sourceAmount;
        this.targetAmount = sourceAmount * exchangeRate;
        this.transactionDate = transactionDate;
        this.timeStamp = timeStamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSourceCurrency() {
        return sourceCurrency;
    }

    public void setSourceCurrency(String sourceCurrency) {
        this.sourceCurrency = sourceCurrency;
    }

    public String getTargetCurrency() {
        return targetCurrency;
    }

    public void setTargetCurrency(String targetCurrency) {
        this.targetCurrency = targetCurrency;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public double getSourceAmount() {
        return sourceAmount;
    }

    public void setSourceAmount(double sourceAmount) {
        this.sourceAmount = sourceAmount;
    }

    public double getTargetAmount() {
        return targetAmount;
    }

    public void setTargetAmount(double targetAmount) {
        this.targetAmount = targetAmount;
    }

    public LocalDate getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(LocalDate transactionDate) {
        this.transactionDate = transactionDate;
    }

    public LocalTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sourceCurrency, targetCurrency, sourceAmount, targetAmount, exchangeRate, transactionDate);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Conversion conv = (Conversion) obj;
        return id == conv.id && sourceCurrency.equals(conv.sourceCurrency) &&
                targetCurrency.equals(conv.targetCurrency) && exchangeRate == conv.exchangeRate &&
                sourceAmount == conv.sourceAmount && targetAmount == conv.targetAmount && transactionDate.equals(conv.transactionDate);

    }
}
