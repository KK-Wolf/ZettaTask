package com.example.zettatask.repositories;

import com.example.zettatask.models.Conversion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface ConversionRepository
        extends CrudRepository<Conversion, Integer>, PagingAndSortingRepository<Conversion, Integer> {
    Page<Conversion> findByTransactionDate(LocalDate transactionDate, Pageable pageable);
}
