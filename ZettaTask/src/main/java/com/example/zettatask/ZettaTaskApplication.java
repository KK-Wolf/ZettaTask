package com.example.zettatask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZettaTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZettaTaskApplication.class, args);
	}

}
