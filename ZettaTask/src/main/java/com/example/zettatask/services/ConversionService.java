package com.example.zettatask.services;

import com.example.zettatask.models.Conversion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ConversionService {

    void addLatestConversion(Conversion latest);

    Optional<Conversion> getById(int id);

    double getCurrentExchangeRateBetween(String sourceCurrency, String targetCurrency);

    Page<Conversion> getByTransactionDate(LocalDate transactionDate, Pageable pageable);
}
