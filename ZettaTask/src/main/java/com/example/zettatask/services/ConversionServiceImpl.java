package com.example.zettatask.services;

import com.example.zettatask.models.Conversion;
import com.example.zettatask.repositories.ConversionRepository;
import com.example.zettatask.utils.exceptions.UnsupportedCurrencyPairException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Optional;

import static com.example.zettatask.utils.Constants.*;

@Service
public class ConversionServiceImpl implements ConversionService {

     private final ObjectMapper objectMapper;

    private final ConversionRepository conversionRepository;

    @Autowired
    public ConversionServiceImpl(ObjectMapper objectMapper, ConversionRepository conversionRepository) {
        this.objectMapper = objectMapper;
        this.conversionRepository = conversionRepository;
    }

    @Override
    public void addLatestConversion(Conversion latest) {
        conversionRepository.save(latest);
    }

    @Override
    public Optional<Conversion> getById(int id) {
        return conversionRepository.findById(id);
    }

    @Override
    public Page<Conversion> getByTransactionDate(LocalDate transactionDate, Pageable pageable) {
        return conversionRepository.findByTransactionDate(transactionDate, pageable);
    }

    @Override
    public double getCurrentExchangeRateBetween(String sourceCurrency, String targetCurrency) {
        try {
            String link = buildLink(sourceCurrency, targetCurrency);
            JsonNode jsonNode = objectMapper.readTree(new URL(link));

            String result = jsonNode.get(RESULT).asText();
            if (!SUCCESS.equals(result)) {
                throw new UnsupportedCurrencyPairException(FAILED_TO_FETCH_EXCHANGE_RATE_RESULT + result);
            }

            return jsonNode.get(CONVERSION_RATE).asDouble();

        } catch (IOException e) {
            throw new UnsupportedCurrencyPairException(sourceCurrency, targetCurrency);
        }
    }

    private String buildLink(String sourceCurrency, String targetCurrency) {
        return EXTERNAL_API_URL +
                sourceCurrency + "/" + targetCurrency;
    }
}
