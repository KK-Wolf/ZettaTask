package com.example.zettatask.controllers.rest;

import com.example.zettatask.mappers.ConversionMapper;
import com.example.zettatask.models.Conversion;
import com.example.zettatask.models.dto.PageDtoOut;
import com.example.zettatask.services.ConversionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;

import static com.example.zettatask.utils.Constants.*;

@RestController
@RequestMapping("/api/conversion-history")
public class ConversionHistoryRestController {


    private final ConversionService conversionService;
    private final ConversionMapper conversionMapper;

    @Autowired
    public ConversionHistoryRestController(ConversionService conversionHistoryService, ConversionMapper conversionMapper) {
        this.conversionService = conversionHistoryService;
        this.conversionMapper = conversionMapper;
    }

    @GetMapping
    public PageDtoOut getConversionHistory(
            @RequestParam(name = "transactionId", required = false) Integer transactionId,
            @RequestParam(name = "transactionDate", required = false) LocalDate transactionDate,
            Pageable pageable
    ) {
        if (transactionId != null) {
            Optional<Conversion> conversionOptional = conversionService.getById(transactionId);
            if (conversionOptional.isPresent()) {
                Conversion conversion = conversionOptional.get();
                return new PageDtoOut(Collections.singletonList(conversion), pageable.getPageNumber(),
                        pageable.getPageSize(), 1, 1, 1);
            } else {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                        CONVERSION_WITH_ID + transactionId + NOT_FOUND);
            }

        } else if (transactionDate != null) {
            Page<Conversion> result =
                    conversionService.getByTransactionDate(transactionDate, pageable);
            if (result.isEmpty()) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, NO_TRANSACTIONS_FOR_THE_PROVIDED_PARAMETERS);
            }
            return conversionMapper.fromPages(result);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    ONE_OF_PARAMETERS_TRANSACTION_ID_AND_TRANSACTION_DATE_MUST_BE_PROVIDED);
        }
    }


}
