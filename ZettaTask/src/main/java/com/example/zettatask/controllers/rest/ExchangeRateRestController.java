package com.example.zettatask.controllers.rest;

import com.example.zettatask.models.dto.RateDtoOut;
import com.example.zettatask.services.ConversionService;
import com.example.zettatask.utils.exceptions.UnsupportedCurrencyPairException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/exchange-rate")
public class ExchangeRateRestController {
    private final ConversionService conversionService;

    @Autowired
    public ExchangeRateRestController(ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @GetMapping
    public RateDtoOut getCurrentRate(
            @RequestParam(name = "sourceCurrencyCode") String sourceCurrencyCode,
            @RequestParam(name = "targetCurrencyCode") String targetCurrencyCode
    ) {
        try {
            RateDtoOut dtoOut = new RateDtoOut();
            dtoOut.setCurrentRate(conversionService.getCurrentExchangeRateBetween(sourceCurrencyCode,
                    targetCurrencyCode));

            return dtoOut;
        } catch (UnsupportedCurrencyPairException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
