package com.example.zettatask.controllers.rest;

import com.example.zettatask.mappers.ConversionMapper;
import com.example.zettatask.models.Conversion;
import com.example.zettatask.models.dto.SingleConversionDto;
import com.example.zettatask.models.dto.SingleConversionDtoOut;
import com.example.zettatask.services.ConversionService;
import com.example.zettatask.utils.exceptions.UnsupportedCurrencyPairException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/currency-conversion")
public class CurrencyConversionRestController {

    private final ConversionService conversionService;
    private final ConversionMapper conversionMapper;

    @Autowired
    public CurrencyConversionRestController(ConversionService conversionService, ConversionMapper conversionMapper) {
        this.conversionService = conversionService;
        this.conversionMapper = conversionMapper;
    }

    @PostMapping
    public SingleConversionDtoOut create(@RequestBody SingleConversionDto singleConversionDto) {
        try {
            Conversion conversion = conversionMapper.fromDto(singleConversionDto);
            conversionService.addLatestConversion(conversion);
            return conversionMapper.toDto(conversion);

        } catch (UnsupportedCurrencyPairException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


}
