package com.example.zettatask;

import com.example.zettatask.models.Conversion;
import com.example.zettatask.repositories.ConversionRepository;

import com.example.zettatask.services.ConversionServiceImpl;
import com.example.zettatask.utils.exceptions.UnsupportedCurrencyPairException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class ConversionServiceImplTests {
    @Mock
    ObjectMapper objectMapper;
    @Mock
    ConversionRepository conversionRepository;
    @InjectMocks
    ConversionServiceImpl conversionService;

    private Conversion conversion;

    @BeforeEach
    public void create() {
        conversion = TestHelper.createMockConversion();
    }

    @Test
    public void getByIdShouldReturnCorrectConversionWhenTransactionWithProvidedIdExists() {
        Mockito.when(conversionRepository.findById(conversion.getId()))
                .thenReturn(Optional.of(conversion));

        Optional<Conversion> conversion1 = conversionService.getById(conversion.getId());

        Assertions.assertEquals(Optional.of(conversion), conversion1);
    }

    @Test
    public void getByIdShouldReturnEmptyListWhenTransactionWithProvidedIdDoesNotExist() {
        Mockito.when(conversionRepository.findById(Integer.MAX_VALUE))
                .thenReturn(Optional.empty());


        Optional<Conversion> conversion1 = conversionService.getById(Integer.MAX_VALUE);

        Assertions.assertEquals(Optional.empty(), conversion1);
    }

    @Test
    public void addLatestConversionShouldCallRepository() {
        conversionService.addLatestConversion(conversion);

        Mockito.verify(conversionRepository, Mockito.times(1))
                .save(conversion);
    }

    @Test
    public void getTransactionByTransactionDateShouldCallRepository() {
        PageImpl<Conversion> conversionPage = new PageImpl<>(List.of(conversion));

        Mockito.when(conversionRepository.findByTransactionDate(conversion.getTransactionDate(), Pageable.unpaged()))
                .thenReturn(conversionPage);

        Page<Conversion> foundByDate = conversionService.getByTransactionDate(conversion.getTransactionDate(),
                Pageable.unpaged());

        Assertions.assertEquals(conversionPage, foundByDate);
    }

    @Test
    public void getCurrentExchangeRateShouldReturnValidRateWhenArgumentsAreCorrect() throws IOException {

        String jsonResponse = "{\"result\":\"success\",\"conversion_rate\":0.9247}";
        JsonNode jsonNode = new ObjectMapper().readTree(jsonResponse);
        Mockito.when(objectMapper.readTree(Mockito.any(URL.class))).thenReturn(jsonNode);

        String sourceCurrency = "EUR";
        String targetCurrency = "BGN";

        double expectedRate = 0.9247;

        double actualRate = conversionService.getCurrentExchangeRateBetween(sourceCurrency, targetCurrency);

        Assertions.assertEquals(expectedRate, actualRate);
    }

    @Test
    public void getCurrentExchangeRateShouldThrowUnsupportedCurrencyPairExceptionWhenArgumentsAreNotCorrect()
            throws IOException {
        String sourceCurrency = "ABV";
        String targetCurrency = "ABC";

        String jsonResponse = "{\"result\":\"error\"}";
        JsonNode jsonNode = new ObjectMapper().readTree(jsonResponse);
        Mockito.when(objectMapper.readTree(Mockito.any(URL.class))).thenReturn(jsonNode);

        Assertions.assertThrows(
                RuntimeException.class,
                () -> conversionService.getCurrentExchangeRateBetween(sourceCurrency, targetCurrency));
    }


}
