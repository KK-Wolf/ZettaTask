package com.example.zettatask;

import com.example.zettatask.models.Conversion;

import java.time.LocalDate;

public class TestHelper {
    public static Conversion createMockConversion(){
        var mockConversion = new Conversion();
        mockConversion.setSourceCurrency("BGN");
        mockConversion.setTargetCurrency("EUR");
        mockConversion.setSourceAmount(100);
        mockConversion.setId(1);
        mockConversion.setExchangeRate(1.95);
        mockConversion.setTargetAmount(195);
        mockConversion.setTransactionDate(LocalDate.of(2024,03,23));

        return mockConversion;
    }

}
