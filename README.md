# Zetta Java Assignement



## Getting started

You are currently looking at a simple foreign exchange application, which is one of the most common services used in financial applications. It has an Exchange Rate Endpoint, a Currency Conversion Endpoint, a Conversion History Endpoint and it utilizes an external service provider for fetching exchange rates and performing the currency conversion calculations. More details are available after running the application and going to the following link: http://localhost:8080/swagger-ui/index.html#/ 
